#include<stdio.h>
#include<stdlib.h>

/*
    Nodo basico de lista enlazada
*/
struct Nodo{
    void  *data;
    struct Node *next;
};

/*
    Funcion para añadir un nodo al inicio de la lista enlazada
    Recibe: la cabeza de la lista, un puntero a la data y el tamaño de la data
*/
void push(struct Nodo** cabeza, void *new_data, size_t data_size)
{
    //Memoria para el nodo
    struct Nodo* new_node = (struct Nodo*)malloc(sizeof(struct Nodo));
    new_node->data  = malloc(data_size);
    new_node->next = (*cabeza);

    // Copy contents of new_data to newly allocated memory.
    // Assumption: char takes 1 byte.
    int i;
    for (i=0; i<data_size; i++)
        *(char *)(new_node->data + i) = *(char *)(new_data + i);

    // Change head pointer as new node is added at the beginning
    (*cabeza)    = new_node;
}

/* Function to print nodes in a given linked list. fpitr is used
   to access the function to be used for printing current node data.
   Note that different data types need different specifier in printf() */


// Function to print an integer
void printInt(void *n)
{
    while (n != NULL){
        printf(" %c", *char n->data);
        n = n->next;
    }

}

/* Driver program to test above function */
int NodoInit()
{
    struct Nodo *start = NULL;

    // Create and print an int linked list
    unsigned int_size = sizeof(int);
    int arr[] = {10, 20, 30, 40, 50}, i;
    for (i=4; i>=0; i--)
        push(&start, &arr[i], int_size);
    printf("Created integer linked list is \n");
    printList(start, printInt);

    // Create and print a float linked list
    unsigned float_size = sizeof(float);
    start = NULL;
    float arr2[] = {10.1, 20.2, 30.3, 40.4, 50.5};
    for (i=4; i>=0; i--)
        push(&start, &arr2[i], float_size);
    printf("\n\nCreated float linked list is \n");

    return 0;
}